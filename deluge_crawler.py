import feedparser
import sqlite3
import sys
import os.path
from time import sleep
import datetime
import urllib2

# also see news_shift.py (other project folder)

con = None
def get_db():
	global con
	if not con:
		current_dir = os.path.dirname(os.path.realpath(__file__))
		con = sqlite3.connect(os.path.join(current_dir,'newsitems.db'))
		# con.text_factory = str
		con.row_factory = sqlite3.Row
		con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')

	return con

def db_create():
	cur = get_db().cursor()
	cur.execute("CREATE TABLE IF NOT EXISTS `feeds` ( `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `name` TEXT UNIQUE, `url` TEXT UNIQUE );")
	cur.execute("""CREATE TABLE IF NOT EXISTS "feed_items" ( 	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 	`feed_id`	INTEGER, 	`guid`	TEXT UNIQUE, 	FOREIGN KEY(`feed_id`) REFERENCES feeds(id) ON DELETE CASCADE );""")
	cur.execute("""CREATE TABLE 
	"feed_item_versions" (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	`feed_item_id`	INTEGER,
	`title`	TEXT,
	`summary`	TEXT,
	`media_url`	TEXT,
	`created_at`	DATETIME DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY(`feed_item_id`) REFERENCES feed_items(id) ON DELETE CASCADE
	);""")
	feeds = [
		('BBC', 'http://feeds.bbci.co.uk/news/rss.xml'),
		('2', 'http://feeds.foxnews.com/foxnews/latest'),
		('3', 'http://www.japantoday.com/feed'),
		('4', 'http://rt.com/rss/'),
		('5', 'http://www.aljazeera.com/Services/Rss/?PostingId=2007731105943979989'),
		# ('6', 'http://www.voanews.com/api/epiqq'),
		('7', 'http://rss.cbc.ca/lineup/topstories.xml'),
		# ('8', 'http://rss.cbc.ca/lineup/canada.xml'),
		('9', 'http://feeds.abcnews.com/abcnews/topstories'),
		# ('10', 'http://feeds.abcnews.com/abcnews/usheadlines'),
		('11', 'http://feeds.nbcnews.com/feeds/topstories'),
		# ('12', 'http://www.nytimes.com/services/xml/rss/nyt/US.xml'),
		# ('13', 'http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml'),
		# ('14', 'http://feeds.washingtonpost.com/rss/national'),
		# ('15', 'http://www.sabc.co.za/SABC/RSS/news/SouthAfricaRSSFeed.xml'),
		# ('16', 'http://rssfeeds.usatoday.com/usatoday-NewsTopStories'),
		# ('17', 'http://www.sabc.co.za/SABC/RSS/news/AfricaRSSFeed.xml'),
		# ('18', 'http://rssfeeds.usatoday.com/UsatodaycomNation-TopStories'),
		# ('19', 'http://www.miamiherald.com/news/latest-top-stories/index.xml'),
		('20', 'http://feeds.news24.com/articles/news24/SouthAfrica/rss'),
		('21', 'http://www.cbsnews.com/feeds/rss/main.rss'),
		# ('22', 'http://www.cbsnews.com/feeds/rss/national.rss'),
		# ('23', 'http://24.com.feedsportal.com/c/33816/f/607927/index.rss'),
		('24', 'http://rss.cnn.com/rss/cnn_topstories.rss'),
		# ('25', 'http://rss.cnn.com/rss/cnn_us.rss'),
		# ('26', 'http://www.iol.co.za/cmlink/1.621'),
		# ('27', 'http://seattletimes.com/rss/home.xml'),
		# ('28', 'http://www.nation.co.ke/-/1148/1148/-/view/asFeed/-/vtvnjq/-/index.xml'),
		# ('29', 'http://rss.canada.com/get/?F229'),
		# ('30', 'http://www.standardmedia.co.ke/rss/headlines.php'),
		# ('31', 'http://www.thestar.com/feeds.topstories.rss'),
		# ('32', 'http://www.egyptnews.net/index.php/rss/d7006824400aaac1'),
		# ('33', 'http://www.buenosairesherald.com/articles/rss.aspx'),
		# ('34', 'http://allafrica.com/tools/headlines/rdf/tunisia/headlines.rdf'),
		# ('35', 'http://feeds.feedburner.com/TheRioTimes?format=xml'),
		# ('36', 'http://allafrica.com/tools/headlines/rdf/morocco/headlines.rdf'),
		# ('37', 'http://allafrica.com/tools/headlines/rdf/mozambique/headlines.rdf'),
		# ('38', 'http://santiagotimes.cl/feed/'),
		# ('39', 'http://allafrica.com/tools/headlines/rdf/ethiopia/headlines.rdf'),
		# ('40', 'http://allafrica.com/tools/headlines/rdf/equatorialguinea/headlines.rdf'),
		# ('41', 'http://allafrica.com/tools/headlines/rdf/nigeria/headlines.rdf'),
		# ('42', 'http://www.peruthisweek.com/rss'),
		# ('43', 'http://sabahionline.com/en_GB/rss'),
		# ('44', 'http://www.peruviantimes.com/feed/'),
		# ('45', 'http://colombiareports.co/feed/'),
		# ('46', 'http://www.newsroompanama.com/index.php?option=com_ninjarsssyndicator&feed_id=1'),
		# ('47', 'http://www.costaricanewssite.com/feed/'),
		# ('48', 'http://feeds.feedburner.com/nicaraguadispatch/hOaf'),
		# ('49', 'http://www.cubanews.com/feeds/index'),
		# ('50', 'http://www.herald.co.zw/feed/'),
		# ('51', 'http://www.spiegel.de/international/germany/index.rss'),
		# ('52', 'http://www.hararenews.co.zw/feed/'),
		('53', 'http://www.spiegel.de/international/world/index.rss'),
		# ('54', 'http://www.observer.ug/index.php?format=feed&amp;type=rss'),
		# ('56', 'http://rss.dw.de/rdf/rss-en-all'),
		# ('57', 'http://www.france24.com/en/monde/rss'),
		# ('58', 'http://www.france24.com/en/france/rss'),
		# ('59', 'http://www.capitalfm.co.ke/feed/'),
		# ('60', 'http://www.bizcommunity.com/rss/196/11/ct-1.html'),
		# ('61', 'http://en.aswatmasriya.com/rss/'),
		# ('62', 'http://www.newsofrwanda.com/category/english/feed/'),
		('63', 'http://feeds.theguardian.com/theguardian/uk-news/rss'),
		# ('64', 'http://rss.feedsportal.com/c/266/f/3496/index.rss'),
		# ('65', 'http://rss.feedsportal.com/c/266/f/3503/index.rss'),
		# ('66', 'http://news.sky.com/feeds/rss/uk.xml'),
		# ('67', 'http://news.sky.com/feeds/rss/home.xml'),
		# ('68', 'http://feeds.feedburner.com/euronews/en/europa?format=xml'),
		# ('69', 'http://english.pravda.ru/russia/export-articles.xml'),
		# ('70', 'http://english.pravda.ru/world/export-articles.xml'),
		# ('71', 'http://feeds.feedburner.com/themoscowtimes/gAjo?format=xml'),
		# ('72', 'http://leadership.ng/rss?widget_title=&use_pager=0&pager_id=0&sort_by=created&sort_order=DESC'),
		# ('73', 'http://www.portalangop.co.ao/angola/en_us/rss/58830be5-77c5-47b6-8e54-7566bba83104.xml'),
		# ('74', 'http://www.times.co.zm/?feed=rss2'),
		# ('75', 'http://www.xinhuanet.com/english/rss/worldrss.xml'),
		# ('76', 'http://www.xinhuanet.com/english/rss/chinarss.xml'),
		# ('77', 'http://rss.skynews.com.au/c/34485/f/628636/index.rss'),
		# ('78', 'http://www.middle-east-online.com/rss_en/'),
		# ('79', 'http://www.asharq-e.com/rss/news.xml'),
		# ('80', 'http://www.gulf-daily-news.com/LOCALGDN.xml'),
		# ('81', 'http://feeds.feedburner.com/DailyNewsEgypt'),
		# ('82', 'http://www.egyptindependent.com/rss_feed_term/114/rss_en.xml'),
		# ('83', 'http://www.israelnationalnews.com/Rss.aspx?act=0.1'),
		# ('84', 'http://www.iraqinews.com/feed/'),
		# ('85', 'http://www.tehrantimes.com/index.php?option=com_ninjarsssyndicator&feed_id=1&format=raw'),
		# ('86', 'http://news.kuwaittimes.net/feed/'),
		# ('87', 'http://feeds.feedburner.com/TheJordanTimes-LatestNews?format=xml'),
		# ('88', 'http://www.dailystar.com.lb/RSS.aspx?id=1'),
		# ('89', 'http://www.dailystar.com.lb/RSS.aspx?live=1'),
		# ('90', 'http://feeds.feedburner.com/LibyaHerald?format=xml'),
		# ('91', 'http://www.timesofoman.com/rss/Oman.xml'),
		# ('92', 'http://www.timesofoman.com/rss/world.xml'),
		# ('94', 'http://www.maannews.net/eng/Rss.aspx?CID=NEW'),
		# ('95', 'http://cs.gulf-times.com/GulfTimesNewsWebsite/rss.aspx?PortalName=Gulftimes&SectionName=Default&NewsCat=178&AppearOnHomePage=true'),
		# ('96', 'http://www.arabnews.com/cat/1/rss.xml'),
		# ('97', 'http://www.arabnews.com/cat/2/rss.xml'),
		# ('98', 'http://www.emirates247.com/cmlink/en/dubai-chamber-featured-news-1.517982?ot=ot.AjaxPageLayout'),
		# ('99', 'http://www.khaleejtimes.com/services/rss/nation/rss.xml'),
		# ('100', 'http://feeds.feedburner.com/TheAustralianNewsNDM?format=xml'),
		# ('101', 'http://rss.feedsportal.com/c/34697/fe.ed/feeds.smh.com.au/rssheadlines/top.xml'),
		# ('102', 'http://d.yimg.com/au.rss.news.yahoo.com/thewest/wa.xml'),
		# ('103', 'http://feeds.news.com.au/heraldsun/rss/heraldsun_news_topstories_2803.xml'),
		# ('104', 'http://rss.feedsportal.com/c/34702/fe.ed/www.canberratimes.com.au/rssheadlines/top.xml'),
		# ('105', 'http://rss.nzherald.co.nz/rss/xml/nzhrsscid_000000001.xml'),
		# ('106', 'http://www.odt.co.nz/news/feed'),
		# ('107', 'http://feeds.feedburner.com/FijiLiveNews?format=xml'),
		# ('108', 'http://www.europeanvoice.com/rss/6.xml'),
		# ('109', 'http://feeds.euobserver.com/rss/9'),
		# ('110', 'http://daily.tportal.hr/rss/dailynaslovnicarss.xml'),
		# ('111', 'http://praguemonitor.com/rss/1+11+12+13+14+19+143/feed'),
		# ('112', 'http://www.english.rfi.fr/france/rss'),
		# ('113', 'http://www.theirishworld.com/?feed=rss2'),
		# ('114', 'http://www.dutchnews.nl/news/index.xml'),
		# ('115', 'http://feeds.feedburner.com/The-Foreigner?format=xml'),
		# ('116', 'http://feeds.feedburner.com/Romania-Insider?format=xml'),
		# ('117', 'http://feeds.feedburner.com/sptimes?format=xml'),
		# ('118', 'http://rbth.co.uk/xml/index.xml'),
		# ('119', 'http://www.stockholmnews.com/rss.aspx'),
		# ('120', 'http://www.sloveniatimes.com/rss?category_id=1'),
		# ('121', 'http://www.lithuaniatribune.com/feed/'),
		# ('122', 'http://enews.fergananews.com/news.xml'),
		# ('123', 'http://thebangladeshtoday.com/feed/'),
		# ('124', 'http://www.bjreview.com.cn/rss/rss.xml'),
		# ('125', 'http://www.china.org.cn/rss/1201719.xml'),
		# ('126', 'http://www.telegraphindia.com/feeds/rss.jsp?id=4'),
		# ('127', 'http://www.thehindu.com/news/?service=rss'),
		# ('128', 'http://newsonjapan.com/rss/top.xml'),
		# ('129', 'http://vietnamnews.feedsportal.com/c/35237/f/655031/index.rss'),
		# ('130', 'http://www.taiwannews.com.tw/rss/news_headfocus_eng.xml'),
		# ('131', 'http://www.epakistannews.com/feed'),
		# ('132', 'http://kazakh-tv.kz/en/rss'),
	]

	# insert default feeds
	cur.executemany("INSERT INTO feeds (name, url) VALUES (?,?)", feeds)
	get_db().commit()


def get_image_from_feed(media_thumbnails):
	'''Get the biggest thumbnail from the set'''
	if len(media_thumbnails) < 1:
		return media_thumbnails

	max_thumb = None

	for thumb in media_thumbnails:
		if not max_thumb:
			max_thumb = thumb
			continue

		if thumb['width'] > max_thumb['width']:
			max_thumb = thumb

	return max_thumb

def save_item(feed,item):
	# check if it exists in the database
	cur.execute("SELECT * FROM feed_items WHERE feed_id = ? AND guid = ?", (feed['id'], item['guid']))
	result = cur.fetchone()
	# if not, create a feed_item
	if not result:
		cur.execute("INSERT INTO feed_items (feed_id, guid) VALUES (?, ?)", (feed['id'], item['guid']))
		get_db().commit()
		item_id = cur.lastrowid
		existed = False
		# print '\tnew', item['title'],'\n'
	else:
		item_id = result['id']
		existed = True

	item['id'] = item_id
	
	# then check if it has changed from the latest version and if so create new version
	cur.execute("SELECT * FROM feed_item_versions WHERE feed_item_id = ? ORDER BY created_at DESC LIMIT 1", (item_id,) )
	version = cur.fetchone()
	
	if not version or (version['title'] != item['title'] or version['summary'] != item['summary'] or version['media_url'] != item['media_url']):
		cur.execute(
			"INSERT INTO feed_item_versions (feed_item_id, title, summary, media_url) VALUES (?, ?, ?, ?)",
			(item_id, item['title'], item['summary'], item['media_url'])
			)
		get_db().commit()
		item['version_id'] = cur.lastrowid
		msg = 'Updated' if existed else 'Saved'
		print '\t' + msg, item['title']
		
		# notify server!
		try:
			urllib2.urlopen('http://localhost:8888/api?item_id=' + str(item['id']))
		except Exception, e:
			print "\t", e # notifty but continue if server is down
	else:
		item['version_id'] = version['id']
		# print '\tchecked', item['title'],'\n'

	return item


def read_feed(feed):
	for entry in feedparser.parse(feed["url"]).entries:
		# print entry
		# No link? Skip
		# print entry
		if not hasattr(entry, 'id'):
			if not hasattr(entry, 'link'):
				print '\tNO ID!'
				continue
			else:
				guid = entry.link
		else:
			guid = entry.id

		item = {
			'guid': guid,
			'title': entry.title,
			'summary': entry.summary,
		}

		# print item

		if 'media_thumbnail' in entry:
			thumb = get_image_from_feed(entry.media_thumbnail)
			item['media_url'] = thumb['url']
		else:
			item['media_url'] = ''

		item = save_item(feed,item)
	
	return True		

if __name__ == '__main__':
	if not os.path.isfile('newsitems.db'):
		db_create()

	while True:
		cur = get_db().cursor()
		cur.execute("SELECT id, name, url FROM feeds")
		feeds = cur.fetchall()
		count = len(feeds)
		i = 1
		for feed in feeds:
			header = "%s/%s: %s (%s)" % (i,count,feed['name'],feed['url'])
			print header
			print '='*len(header)
			try:
				read_feed(feed)
			except Exception, e:
				print e
			i+=1

		print datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), 'Done loop, sleep a minute...'
		sleep(16)
		