#!/bin/python
import os
from PCV.localdescriptors import sift
from glob import glob

from PIL import Image, ImageDraw
# from numpy import ndindex,nditer, array_equal, array_equiv

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Use ie. --item1-id 2 item2-id 11575')
	parser.add_argument('--item1', required=True, type=str, help='First image')
	parser.add_argument('--item2', required=True, type=str, help='Second image in hte comparison')

	parser.add_argument('--target-file', type=str, help='filename to save output to. If non existent, output is shown')
	parser.add_argument('--target-width', default=200, type=int, help='Max width of each thumb')

	args = parser.parse_args()





	im1 = Image.open(args.item1)
	im2 = Image.open(args.item2)

	featfile1 = "%ssift" % args.item1[:-3]
	featfile2 = "%ssift" % args.item2[:-3]

	l1,d1 = sift.read_features_from_file(featfile1)
	l2,d2 = sift.read_features_from_file(featfile2)

	matches = sift.match_twosided(d1,d2)
	nbr_matches = sum(matches > 0)
	keys = matches.nonzero()[0]

	print nbr_matches,matches
	print len(l1)
	img_scale = float(args.target_width) / im1.size[0] # Scale for 'antialiassing'
	im1 = im1.resize((int(im1.size[0] * img_scale), int(im1.size[1]*img_scale)))
	draw1 = ImageDraw.Draw(im1)
	for key in keys:
		print l1[key][2]
		print l1[key][:2][0]
		print l1[key][:2][1]
		print l1[key]
		print '-----'
		r = l1[key][2] * img_scale
		x = l1[key][:2][0] * img_scale
		y = l1[key][:2][1] * img_scale
		draw1.ellipse((x-r, y-r, x+r, y+r), None, 'red')
		print key
		print l1[key]

	matches = sift.match_twosided(d2,d1)
	nbr_matches = sum(matches > 0)
	keys = matches.nonzero()[0]

	print nbr_matches,matches
	print len(l2)
	img_scale = float(args.target_width) / im2.size[0] # Scale for 'antialiassing'
	im2 = im2.resize((int(im2.size[0] * img_scale), int(im2.size[1]*img_scale)))
	draw2 = ImageDraw.Draw(im2)
	for key in keys:
		r = l1[key][2] * img_scale
		x = l1[key][:2][0] * img_scale
		y = l1[key][:2][1] * img_scale
		draw2.ellipse((x-r, y-r, x+r, y+r), None, 'red')
		print key
		print l2[key]



	new_im = Image.new('RGB', (args.target_width*2+10, max(im1.size[1], im2.size[1])))

	#Here I resize my opened image, so it is no bigger than 100,100
	# im1.thumbnail((200,200))
	# im2.thumbnail((200,200))
	new_im.paste(im1, (0,0))
	new_im.paste(im2, (args.target_width+10,0))

	if not args.target_file:
		new_im.show()
	else:
		new_im.save(args.target_file)

	# for p in feat_locs:
	# 	r = p[2] * img_scale
	# 	# t = arange(0,1.01,.01)*2*pi
	# 	x = p[:2][0] * img_scale
	# 	y = p[:2][1] * img_scale
	# 	# x = r*cos(t) + p[:2][0]
	# 	# y = r*sin(t) + p[:2][1]
	# 	draw.ellipse((x-r, y-r, x+r, y+r), None, 'red')
	# matches_12 = sift.match(d1,d2)
	# matches_21 = sift.match(d2,d1)
	# for n in matches_12.nonzero()[0]:
	# 	# print n, int(matches_12[n]), matches_21[int(matches_12[n])]
	# 	if matches_21[n] != int(matches_12[n]):
	# 		print "FOUND!"
	# 		print featfile, ff
	# 		matches_12[n] = 0
	# 		exit(1)
			
	# 		# i=0
	# 		# for match in matches:
	# 		# 	if match:
	# 		# 		# print l1[0]
	# 		# 		print "MATCH!", match
	# 		# 		print d1[i]
	# 		# 		for d in d2:
	# 		# 		 if array_equiv(d,d1[i]):
	# 		# 			print "MATCH d1[i]!"

	# 		# 		print d2[i]
	# 		# 		for d in d1:
	# 		# 		 if array_equiv(d,d2[i]):
	# 		# 			print "MATCH d2[i]!"

	# 		# 		print "---------"
	# 		# 		exit(1)
	# 		# 	i+=1
	# 		# print "---------"
