#!/bin/python
import json, sqlite3, os
from PCV.localdescriptors import sift
from glob import glob
from numpy import ndindex,nditer, array_equal, array_equiv
import subprocess

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='bla.')
	parser.add_argument('--item-id', default=False, type=int, help='The feed item id')

	args = parser.parse_args()

	featfiles = glob(os.path.join('images', "*.sift"))

	if args.item_id:
		featfile = os.path.join('images',"%s.sift" % item_id)
	else:
		featfile = featfiles[0]

	print featfile

	l1,d1 = sift.read_features_from_file(featfile)

	for ff in featfiles:
		if ff == featfile:
			continue

		print ff
		l2,d2 	= sift.read_features_from_file(ff)
		matches = sift.match_twosided(d1,d2)
		nbr_matches = sum(matches > 0)
		# print 'number of matches = ', nbr_matches 
		# TODO: store matches (and non-matches to avoid recheck?)
		
		if(nbr_matches):
			file1 = featfile[:-5]+'.jpg'
			file2 = ff[:-5]+'.jpg'
			outfile = os.path.join('output', featfile[7:-5] +'-'+ff[7:-5] +'.jpg' )
			subprocess.call(['python','find_match.py','--item1',file1,'--item2',file2,'--target-file',outfile])
			
			
			# i=0
			# for match in matches:
			# 	if match:
			# 		# print l1[0]
			# 		print "MATCH!", match
			# 		print d1[i]
			# 		for d in d2:
			# 		 if array_equiv(d,d1[i]):
			# 			print "MATCH d1[i]!"

			# 		print d2[i]
			# 		for d in d1:
			# 		 if array_equiv(d,d2[i]):
			# 			print "MATCH d2[i]!"

			# 		print "---------"
			# 		exit(1)
			# 	i+=1
			# print "---------"
