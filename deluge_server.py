from tornado import websocket, web, ioloop
import json, sqlite3, os

# image parser
from PCV.localdescriptors import sift
from PIL import Image, ImageDraw
from numpy import cos,sin

import subprocess32 as subprocess
from glob import glob
from shutil import copyfile

# Big thanks to: https://github.com/hiroakis/tornado-websocket-example
# and https://gitorious.org/pzi-prototyping/the-image-network

clients = []

current_popen = None

current_item_id = None
current_dir = os.path.dirname(os.path.realpath(__file__))

con = None
def get_db():
	global con
	if not con:
		con = sqlite3.connect(os.path.join(current_dir,'newsitems.db'))
		# con.text_factory = str
		con.row_factory = sqlite3.Row
		con.text_factory = lambda x: unicode(x, 'utf-8', 'ignore')
	return con

def clients_send(data):
	# print 'send', data
	data = json.dumps(data)
	for c in clients:
		c.write_message(data)

def download_image(imagename, url):
	import pycurl
	from StringIO import StringIO

	print '\tDownload ', imagename , 'from', url
	output = StringIO()
	conn = pycurl.Curl()
	conn.setopt(conn.URL, str(url))
	conn.setopt(conn.FOLLOWLOCATION, 1)
	conn.setopt(conn.USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.97 Safari/537.11')
	conn.setopt(conn.WRITEFUNCTION, output.write)
	conn.perform()
	code = conn.getinfo(conn.RESPONSE_CODE)
	conn.close()
	if code != 200:
		print '\t\tError! Code:', code
		return False

	# resize if it's too big
	img = Image.open(StringIO(output.getvalue()))
	size = img.size
	if size[0] > 300 or size[1] > 300:
		if size[0] > size[1]:
			factor = float(300) / size[0]
		else:
			factor = float(300) / size[1]

		new_size = (int(size[0]* factor), int(size[1] * factor))
		print '\t\tDownscale to', new_size, factor, size
		img = img.resize(new_size, Image.LANCZOS)

	# f = open(imagename, 'w')
	img.save(imagename)

	img.close()

	return imagename

def fetch_item(item_id):
	cur = get_db().cursor()
	cur.execute("SELECT fi.id, fiv.id as feed_item_version_id, fiv.media_url, fiv.title, fiv.summary as summary, fiv.created_at as created_at FROM feed_items fi INNER JOIN feed_item_versions fiv ON fiv.feed_item_id = fi.id WHERE fi.id = ? ORDER BY fiv.id DESC LIMIT 1", (item_id,))
	return cur.fetchone()


def prepare_item_images(item):
	if not item or not item['media_url']:
		return False

	# the paths for the version image
	imagename 	 = os.path.join('images','versions',str(item['feed_item_version_id'])+'.jpg')
	featfile 	 = os.path.join('images','versions',str(item['feed_item_version_id'])+'.sift')
	featfile_img = featfile + '.png'

	# download image
	if not os.path.exists(imagename):
		if not download_image(imagename, item['media_url']):
			print '\tDownload error'
			return False
	
	# parse sift
	if not os.path.exists(featfile):
		sift.process_image(imagename, featfile)

	# generate sift image
	if not os.path.exists(featfile_img):
		try:
			feat_locs, feat_descs = sift.read_features_from_file(featfile)
		except Exception, e:
			print 'WARNING, SIFT WENT WRONG!'
			print e
			return False

		im0 = Image.open(imagename)

		img_scale = float(300) / im0.size[0] # Scale for 'antialiassing'

		im1 = im0.resize((int(im0.size[0] * img_scale), int(im0.size[1]*img_scale)))
		draw = ImageDraw.Draw(im1)
		for p in feat_locs:
			r = p[2] * img_scale
			# t = arange(0,1.01,.01)*2*pi
			x = p[:2][0] * img_scale
			y = p[:2][1] * img_scale
			# x = r*cos(t) + p[:2][0]
			# y = r*sin(t) + p[:2][1]
			draw.ellipse((x-r, y-r, x+r, y+r), None, 'red')

		im1.save(featfile_img)


	item_image 	 = os.path.join('images',str(item['id'])+'.jpg')
	item_feats 	 = os.path.join('images',str(item['id'])+'.sift')
	item_fimage	 = item_feats + '.png'

	# create symlinks for the current items
	if os.path.islink(item_image):
		os.unlink(item_image)
	os.symlink(imagename[7:],	 item_image)

	if os.path.islink(item_feats):
		os.unlink(item_feats)
	os.symlink(featfile[7:],	 item_feats)

	if os.path.islink(item_fimage):
		os.unlink(item_fimage)
	os.symlink(featfile_img[7:], item_fimage)

	# finaly send the names of the ITEM as return value (so not the version)
	return (item_image, item_feats, item_fimage)


class SubHandlers():
	def send(self,data):
		import websocket as webs
		# if data is false, do nothing
		if not data:
			return False;

		ws = webs.WebSocket()
		ws.connect('ws://localhost:8888/ws')
		ws.send(json.dumps(data))
		ws.close()
		return True

	def get_sift(self, item_id):
		item = fetch_item(item_id)
		if not item or not item['media_url']:
			return False

		# notify clients of new item
		self.send({'action':'new_item', 'media_url':item['media_url'], 'title': item['title'], 'feed_item_id': item_id})
		current_item_id = item_id

		print 'Start item:', item_id, item['title']

		info = prepare_item_images(item)

		if not info:
			return False

		imagename, featfile, featfile_img = info

		# # Download image
		# imagename = os.path.join('images',str(item_id)+'.jpg')
		# if not os.path.exists(imagename):
		# 	if not download_image(imagename, item['media_url']):
		# 		return False
		
		# # parse sift
		# featfile = os.path.join('images',str(item_id)+'.sift')
		# if not os.path.exists(featfile):
		# 	sift.process_image(imagename, featfile)

		# # create sift image:
		# featfile_img = featfile + '.png'
		# if not os.path.exists(featfile_img):
		# 	feat_locs, feat_descs = sift.read_features_from_file(featfile)

		# 	im0 = Image.open(imagename)

		# 	img_scale = float(300) / im0.size[0] # Scale for 'antialiassing'

		# 	im1 = im0.resize((int(im0.size[0] * img_scale), int(im0.size[1]*img_scale)))
		# 	draw = ImageDraw.Draw(im1)
		# 	for p in feat_locs:
		# 		r = p[2] * img_scale
		# 		# t = arange(0,1.01,.01)*2*pi
		# 		x = p[:2][0] * img_scale
		# 		y = p[:2][1] * img_scale
		# 		# x = r*cos(t) + p[:2][0]
		# 		# y = r*sin(t) + p[:2][1]
		# 		draw.ellipse((x-r, y-r, x+r, y+r), None, 'red')

		# 	im1.save(featfile_img)

		self.send({'action':'sift1', 'image_url':featfile_img, 'feed_item_id': item_id})

		# find comparible images:

		l1,d1 = sift.read_features_from_file(featfile)

		featfiles = glob(os.path.join('images', "*.sift"))
		for ff in featfiles:
			if ff == featfile:
				continue

			l2,d2 	= sift.read_features_from_file(ff)
			matches = sift.match_twosided(d1,d2)
			nbr_matches = sum(matches > 0)
			print 'number of matches = ', nbr_matches 
			# TODO: store matches (and non-matches to avoid recheck?)
			
			if(nbr_matches):
				match_item_id = int(ff[7:-5])
				match_item = fetch_item(match_item_id)

				ff_img = ff[:-4] + 'jpg'
				ff_img = ff + '.png'
				self.send({'action':'sift_match', 'image_url':ff_img, 'feed_item_id': item_id, 'match_item_id': match_item_id, 'matches': nbr_matches, 'title': match_item['title'], 'summary':match_item['summary'], 'created_at':match_item['created_at']})
		return True


class IndexHandler(web.RequestHandler):
	def get(self):
		self.render("www/index4.html")

class SocketHandler(websocket.WebSocketHandler):
	def check_origin(self, origin):
		return True

	def open(self):
		if self not in clients:
			clients.append(self)

	def on_message(self, msg):
		msg = json.loads(msg)
		print 'receive:', msg
		clients_send(msg)

	def on_close(self):
		if self in clients:
			clients.remove(self)


class ApiHandler(web.RequestHandler):

	@web.asynchronous
	def get(self, *args):
		global current_popen

		item_id = int(self.get_argument("item_id"))
		
		if not item_id:
			raise web.HTTPError(404)

		
		# Download image and parse sift so this is ALWAYS done when crawling the pages (even when it's going fast)
		item = fetch_item(item_id)
		if not prepare_item_images(item):
			raise web.HTTPError(503)

		self.finish()

		# call external process that communicates with the server so it can run in parallel (non-blocking)
		
		# stop processes if they're running already
		if current_popen:
			print 'Stop running process:', current_popen.pid
			current_popen.terminate()

		current_popen = subprocess.Popen(['python','deluge_server.py','--sift','--item-id',str(item_id)])


	@web.asynchronous
	def post(self):
		pass

if __name__ == '__main__':
	import argparse
	parser = argparse.ArgumentParser(description='Generate a "supercut" of one or more video files by searching through subtitle tracks.')
	parser.add_argument('--sift', action='store_true', default=False, help='Do a sift caluclation')
	parser.add_argument('--item-id', default=False, type=int, help='The feed item id')

	args = parser.parse_args()

	if args.sift:
		if not args.item_id:
			print "No item_id given"

		handler = SubHandlers();
		handler.get_sift(args.item_id)
	else:
		print "Start webserver"
		app = web.Application([
			(r'/', IndexHandler),
			(r'/ws', SocketHandler),
			(r'/api', ApiHandler),
			# (r'/(favicon.ico)', web.StaticFileHandler, {'path': '../'}),
			(r'/images/(.*)', web.StaticFileHandler, {'path': './images/'}),
			(r'/js/(.*)', web.StaticFileHandler, {'path': './www/js/'}),
			(r'/(jquery-2.1.3.min.js)', web.StaticFileHandler, {'path': './www/'}),
			(r'/(jquery.shuffle.modernizr.min.js)', web.StaticFileHandler, {'path': './www/Shuffle/dist/'}),
			(r'/(jquery.shuffle.js)', web.StaticFileHandler, {'path': './www/Shuffle/dist/'}),
			(r'/(thetimes.css)', web.StaticFileHandler, {'path': './www/'}),
		])

		app.listen(8888)
		ioloop.IOLoop.instance().start()
