#!/bin/python
import os
from deluge_server import get_db, prepare_item_images
'''
Cleanup from an invalid feed
'''

if __name__ == '__main__':
	cur = get_db().cursor()
	cur.execute("""SELECT fi.id as item_id, fiv.id as version_id FROM feeds f  
	INNER JOIN feed_items fi ON fi.feed_id = f.id 
	LEFT JOIN feed_item_versions fiv ON fiv.feed_item_id = fi.id
	LEFT JOIN feed_item_relations fir ON fir.feed_item_id = fi.id

	LEFT JOIN feed_item_relation_data fird ON fird.feed_item_relation_id = fir.id
	LEFT JOIN feed_item_relation_cache firc ON firc.feed_item_relation_id = fir.id
WHERE f.id = 7;""")

	n = 0
	while True:
		i = cur.fetchone()
		if i == None:
			break

		n+=1
		item_id = i['item_id']
		version_id = i['version_id']
		print n, " Item: %s, version: %s" % (item_id, version_id)

		imagename 	 = os.path.join('images','versions',str(version_id)+'.jpg')
		featfile 	 = os.path.join('images','versions',str(version_id)+'.sift')
		featfile_img = featfile + '.png'
		item_image 	 = os.path.join('images',str(item_id)+'.jpg')
		item_feats 	 = os.path.join('images',str(item_id)+'.sift')
		item_fimage	 = item_feats + '.png'

		# remove symbolic links
		if os.path.islink(item_image):
			print '\texist:', item_image
			os.unlink(item_image)
		if os.path.islink(item_feats):
			print '\texist:', item_feats
			os.unlink(item_feats)
		if os.path.islink(item_fimage):
			print '\texist:', item_fimage
			os.unlink(item_fimage)

		# remove versions
		if os.path.islink(imagename):
			print '\texist:', imagename
			os.unlink(imagename)
		if os.path.islink(featfile):
			print '\texist:', featfile
			os.unlink(featfile)
		if os.path.islink(featfile_img):
			print '\texist:', featfile_img
			os.unlink(featfile_img)
		

# After this run:

"""PRAGMA foreign_keys=ON;DELETE FROM feeds WHERE feed_id = 7;"""

















