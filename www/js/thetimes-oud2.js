$(function(){
	var elements = []
	var max_weight = 0;
	var options = {
		gridSizeX: 4,
		gridSizeY: 4,
		// maxSizeX: 2,
		// maxSizeY: 2,
		sizes: [
			{className: 'times-item-l', minWeight: 25, maxOcc: 1, size: 4, w:2, h:2},
			{className: 'times-item-s--w', minWeight: 10, maxOcc: 1, size: 3, w:2, h:1},
			{className: 'times-item-s--h', minWeight: 5, maxOcc: 1, size: 2, w:1, h:2},
			{className: 'times-item-xs', minWeight: 1, size: 1, w: 1, h:1}
		]
	}

	// string of classnames
	var classNames = '';
	for (var i = options['sizes'].length - 1; i >= 0; i--) {
		classNames += ' ' + options['sizes'][i]['className'];
	};

	var $wrapper = $('#wrapper');

	var elSorter = function(a,b)
	{
		a = $(a).data('weight');
		b = $(b).data('weight');
		return a < b ? 1 : -1;
	}
		

	var removeElement = function($element)
	{
		console.log('remove');
		$elements.each(function(index, el){
			$el = $(el);
			if($element.is(el))
			{
				console.log('r', index, el);
				$el.find('p').html('removed!'); // TEST
				// remove elements if needed
				$el.remove();
				elements.splice(index, 1);
			}
		});
	}

	var addElement = function($element)
	{
		elements.push($element);
		$elements = $(elements);

		// max_weight deprecated? or do class determination based on this (with a logarithmic scale or something)		
		// if($element.data('weight') > max_weight) max_weight = $element.data('weight');

		$elements = $elements.sort(elSorter);

		assignedClasses = [];
		maxPlaces = options['gridSizeX'] * options['gridSizeY'];
		usePlaces = 0;
		// create array in pythony way
		for (var i = options['sizes'].length - 1; i >= 0; i--) { assignedClasses[i] = 0; }

		doAdd = true;
		// determine sizes
		$elements.each(function(index, el){
			$el = $(el);

			weight = $el.data('weight');

			isUsed = false;
			
			$.each(options['sizes'],function(i, size){
				// console.log(el, size);
				if(weight >= size['minWeight'])
				{
					// skip if there are too many of this size:
					if(typeof size['maxOcc'] != 'undefined' && assignedClasses[i] >= size['maxOcc'])
					{	
						return true; // continue
					}

					// avoid too big items to be added! Add it as a smaller one
					if(usePlaces + size['size'] > maxPlaces)
						return true; // continue

					// the current size is being used!
					usePlaces += size['size'];
					console.log('places',usePlaces);
					isUsed = true;
					assignedClasses[i] += 1;

					if(!$el.hasClass(size['className']))
					{
						console.log('removeclass', el, size['className']);
						$el.removeClass(classNames).addClass(size['className']);
						$el.data('size', size);
						// make packery attempt to keep the item in place
						// fit_x = undefined;
						// fit_y=undefined;
						// if(size['className'] == 'times-item-l')
						// 	fit_x=fit_y=0;

						// $wrapper.packery( 'fit', el , fit_x, fit_y);
						console.log('FOUND!',el, size['className']);
					}
					return false; // break
				}
			});

			if(!isUsed)
			{
				removeElement($el);
				if($element.is($el))
					doAdd = false;
			}
		});

		if(doAdd)
		{
			$wrapper.append( $element );
		}
		else
		{
			console.log('not added',$element);
		}

		layoutGrid();

	}

	var findPosForSize = function(size)
	{
		w = size['w'];
		h = size['h'];
		// walk trough the y and x axes (check per row)
		for (var y = 0; y < grid.length; y--) {
			if(y-1+h > grid.length)
				continue;
			for (var x = 0; x < grid[y].length; x--) {
				if(x-1+w > grid.length)
					continue;
				
				noFit = false;
				// check if there is still place in position:
				for(pos_x = 0; pos_x < w; pos_x++)
				{
					for(pos_y = 0; pos_y < h; pos_y++)
					{
						if(grid[y+pos_y][x+pos_x] > 0)
						{
							noFit = true;
							break;
						}

					}
				}
				// no fit? move to next position
				if(noFit)
					continue;

				//fit? return coordinates:
				return {'x':x,'y':y};
			};
		};
		return false;
	}

	var grid = [];
	for (var y = options['gridSizeX'] - 1; y >= 0; y--) {
		grid[y] = [];
		for (var x = options['gridSizeY'] - 1; x >= 0; x--) {
			grid[y][x] = 0;
		}
	};

	var positionEl = function(element, pos)
	{
		if(!pos)
			removeElement(element);

		$el = $(element);
		$el.css({'left':pos['x']*25+'%','top':pos['y']*25+'%'});
		for(pos_x = 0; pos_x < size['w']; pos_x++)
		{
			for(pos_y = 0; pos_y < size['h']; pos_y++)
			{
				grid[pos['y']+pos_y][pos['x']+pos_x] = 1;
			}
		}
	}

	var layoutGrid = function()
	{
		$elements = $(elements).sort(elSorter);
		

		// van groot naar klein:
		$elements.each(function(i, el){
			size = $el.data('size');
			pos = findPosForSize(size);
			console.log(pos);
			positionEl(el,pos);
		});
	}

	var initiate = function()
	{
		
	}

	initiate();

	nr = 0;
	$('#add').on('click',function(){
		nr++;
		weight = $("#value").val();
		console.log('Add element', weight);
		$element = $('<div class="times-item" data-id="'+nr+'" data-weight="'+weight+'">').data('weight', weight);
		$element.html("<h2>"+nr+"Dit is een test</h2><p>lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, .</p>");
		addElement($element);
	});
});