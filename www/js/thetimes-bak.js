function position() {
		this.style("left", function(d) { return d.x + "px"; })
		.style("top", function(d) { return d.y + "px"; })
		.style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
		.style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
	}
	$( document ).ready(function() {

		var data_items = {
			"name": "items",
			"children": [
				{
	        	}
			]
		};

		function buildTreemap(){

			var margin = {top: 0, right: 1, bottom: 1, left: 0},
			width = $( document ).innerWidth() - margin.left - margin.right,
			height = $( document ).innerHeight() - margin.top - margin.bottom;

			var treemap = d3.layout.treemap()
			.size([width, height])
			.sticky(false)
			.sort(function(a,b) { return a.weight - b.weight; })
			.round(true)
			.value(function(d) { return Math.log(d.weight); });


			var div = d3.select("#wrapper").append("div")
			.style("position", "relative")
			.style("width", (width + margin.left + margin.right) + "px")
			.style("height", (height + margin.top + margin.bottom) + "px")
			.style("left", margin.left + "px")
			.style("top", margin.top + "px")
			.attr("id", "first")
			;

			
			updateItems = function()
			{
				var node = div.datum(data_items).selectAll(".node")
				.data(treemap.nodes);

				node.enter().append("div")
				.attr("class", "node");

				set_content = function(d)
				{ 
					if( d.children )
						return "";
					// determine class based on width & height!
					width = d.dx;
					height = d.dy;
					return "<h2>"+d.title+"</h2><p>"+d.summary+"</p>";
				}

	            // node.style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	            node
	        	// .style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	        	.html(set_content)
	        	// .append("test").html("<i>Tests</i>");

	        	node
	        		// .transition().duration(500)
	        		.call(position)
	        	// node.call(position)
	        	;

	        	node.html(set_content);

	        }
	        // updateItems();

	        nr = 0;
	        d3.selectAll("#add").on("click", function() {
	        	nr++;
	        	console.log(data_items);
	        	appendMatch({
	        		"feed_item_id": nr,
	        		"matches": $("#value").val(),
	        		"summary": "meer",
	        		"title": "a",
	        		"img": "test.jpg"
	        	});
	        });

	    };

	    appendMatch = function(r)
	    {
	    	data_items['children'].push({
        		"id": r['feed_item_id'],
        		"weight": r['matches'],
        		"summary": r['summary'],
        		"title": r['title'],
        		"img": r['image_url']
        	});
        	console.log(data_items['children']);

        	max_items = 10;
        	if(data_items['children'].length > max_items)
        	{
        		sorter = function(a,b)
				{
					return a['weight'] - b['weight'];
				}

        		console.log('pre', data_items['children']);
				data_items['children'].sort(sorter);
        		toremove = data_items['children'].length - max_items;

        		console.log('post', data_items['children']);

        		for (var i = toremove - 1; i >= 0; i--) {
        			console.log('removed',data_items['children'].shift());
        		};
        	}
        	updateItems();
	    }

	    buildTreemap();




	    var start_connection = function()
	    {
	    	console.log('Try connection');
	    	conn = new WebSocket('ws://localhost:8888/ws');
	    	conn.onopen = function(e) {
	    		console.log("Connection established!");
	    	};

	    	$main_item = $('#main_item');

		// {"id":"9783","version_id":"22651","title":"Captain America, Star-Lord meet 'true superheroes'","media_url":"http:\/\/i2.cdn.turner.com\/cnn\/dam\/assets\/141023151646-captain-america-top-tease.jpg"}
		conn.onmessage = function(msg) {
				// play_sound();
				
				r = JSON.parse(msg.data);

				if(typeof r['action'] == 'undefined')
				{
					console.error('No Action defined');	
					return false;
				}
				if(r['action'] == 'new_item')
				{
					$item = $("<div class='item' id='item-"+r['feed_item_id']+"'>");
					$img = $("<img>");
					$item.append($img);

					
					$img.attr('src', r['media_url']);
					$img.attr('title', r['title']);
					$img.attr('id', 'image-' + r['feed_item_id']);

					$title = $("<h2>").text(r['title']);
					$item.append($title);

					$main_item.html($item);	

					$main_item.append($("<div class='sift-matches' id='sift-matches-"+r['feed_item_id']+"'>"));
				}
				else if(r['action'] == 'sift1')
				{
					$img = $('<img id="sift-'+r['feed_item_id']+'">').attr('src',r['image_url']);
					console.log($img);
					$('#item-'+r['feed_item_id']).append($img)
				} 
				else if(r['action'] == 'sift_match')
				{
					appendMatch(r);
					// $div = $('<div class="sift-match">');
					// $img = $('<img class="sift-match-img" title='+r['matches']+'>').attr('src',r['image_url']);
					// console.log($img);
					// $('#item-'+r['feed_item_id']).append($img)

					// $title = $("<h4>").text(r['title']);

					// $div.append($img).append($title);
					// $('#sift-matches-'+r['feed_item_id']).append($div);
				} else {
					console.error('unknonw message', r)
				}

				

				// TODO: request matches and get them one by one (as the computer is doing them)
				
				//start parsing
				// parse_get_sift(feed_item);
			};

			// conn.onerror = function(e){
			// 	setTimeout(start_connection, 3000);
			// }
			
			conn.onclose = function(e){
				console.log('Connection closed, retry in 3,5s');
				setTimeout(start_connection, 3500);
			}
		}

		var parse_load_relation = function(feed_item)
		{
			$.ajax({
				dataType: "json",
				url: 'http://localhost:9090/related',
				data: {'item_id': feed_item['id']},
				success: function(r){
					$img = $("<img>");
					
					$('#item-'+r['feed_item_id']).append($img);
					setTimeout(function(){parse_load_relation(this)}.bind(feed_item), 100);
				}
			});

		}
		start_connection();
	});