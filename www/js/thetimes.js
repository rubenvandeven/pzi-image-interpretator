function position() {
		this.style("left", function(d) { return d.x + "px"; })
		.style("top", function(d) { return d.y + "px"; })
		.style("width", function(d) { return Math.max(0, d.dx - 1) + "px"; })
		.style("height", function(d) { return Math.max(0, d.dy - 1) + "px"; });
	}
	$( document ).ready(function() {

		var data_items = {};

		/*
		function buildTreemap(){

			var margin = {top: 0, right: 1, bottom: 1, left: 0},
			width = $( document ).innerWidth() - margin.left - margin.right,
			height = $( document ).innerHeight() - margin.top - margin.bottom;

			var treemap = d3.layout.treemap()
			.size([width, height])
			.sticky(false)
			.sort(function(a,b) { return a.weight - b.weight; })
			.round(true)
			.value(function(d) { return Math.log(d.weight); });


			var div = d3.select("#wrapper").append("div")
			.style("position", "relative")
			.style("width", (width + margin.left + margin.right) + "px")
			.style("height", (height + margin.top + margin.bottom) + "px")
			.style("left", margin.left + "px")
			.style("top", margin.top + "px")
			.attr("id", "first")
			;

			
			updateItems = function()
			{
				var node = div.datum(data_items).selectAll(".node")
				.data(treemap.nodes);

				node.enter().append("div")
				.attr("class", "node");

				set_content = function(d)
				{ 
					if( d.children )
						return "";
					// determine class based on width & height!
					width = d.dx;
					height = d.dy;
					return "<h2>"+d.title+"</h2><p>"+d.summary+"</p>";
				}

	            // node.style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	            node
	        	// .style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	        	.html(set_content)
	        	// .append("test").html("<i>Tests</i>");

	        	node
	        		// .transition().duration(500)
	        		.call(position)
	        	// node.call(position)
	        	;

	        	node.html(set_content);

	        }
	        // updateItems();

	        nr = 0;
	        d3.selectAll("#add").on("click", function() {
	        	nr++;
	        	console.log(data_items);
	        	appendMatch({
	        		"feed_item_id": nr,
	        		"matches": $("#value").val(),
	        		"summary": "meer",
	        		"title": "a",
	        		"img": "test.jpg"
	        	});
	        });

	    };

	    appendMatch = function(r)
	    {
	    	data_items['children'].push({
        		"id": r['feed_item_id'],
        		"weight": r['matches'],
        		"summary": r['summary'],
        		"title": r['title'],
        		"img": r['image_url']
        	});
        	console.log(data_items['children']);

        	max_items = 10;
        	if(data_items['children'].length > max_items)
        	{
        		sorter = function(a,b)
				{
					return a['weight'] - b['weight'];
				}

        		console.log('pre', data_items['children']);
				data_items['children'].sort(sorter);
        		toremove = data_items['children'].length - max_items;

        		console.log('post', data_items['children']);

        		for (var i = toremove - 1; i >= 0; i--) {
        			console.log('removed',data_items['children'].shift());
        		};
        	}
        	updateItems();
	    }

	    buildTreemap();
	    */

	    var treemap = false;

	    //start new treemap for item
	    var createNewTreemap = function($item, $wrapper)
	    {
	    	// remove old treemap
	    	if(treemap != false)
	    	{
	    		$main_item.children('div').each(function(i, el){
	    			if($wrapper.is(el))
	    				return true;
	    			// console.log('remove',$(el), $wrapper);
	    			$(el).remove();
	    		});
	    	}

	    	var margin = {top: 0, right: 1, bottom: 1, left: 0},
			width = $wrapper.innerWidth() - margin.left - margin.right,
			height = $wrapper.innerHeight() - margin.top - margin.bottom;

			data_items = {
				"name": "items",
				"children": [
					{
						"id": 1,
						"source_el": $item,
						"weight": 5
		        	}
				]
			};
			treemap = d3.layout.treemap()
			.size([width, height])
			.sticky(false)
			.sort(function(a,b) { return a.weight - b.weight; })
			.round(true)
			.value(function(d) { return Math.log(d.weight+1);/*log 1 would create 0... so weight 1 should NOT hav that! */ });

			
			var div = d3.select('#'+$wrapper.attr('id')).append("div")
			.style("position", "relative")
			.style("width", (width + margin.left + margin.right) + "px")
			.style("height", (height + margin.top + margin.bottom) + "px")
			.style("left", margin.left + "px")
			.style("top", margin.top + "px")
			.attr("id", "first")
			;

			var svg = div.append("svg")
			      .attr("width", (width + margin.left + margin.right))
			      .attr("height", (height + margin.top + margin.bottom))
			      .style("position", "absolute")
			      .style("left", margin.left + "px")
				  .style("top", margin.top + "px")
			      ;
			
			updateItems = function()
			{
				var node = div.datum(data_items).selectAll(".node")
				.data(treemap.nodes);

				node.enter().append("div")
				.attr("class", "node");

				set_content = function(d)
				{ 
					if( d.children )
						return "";

					

					// determine class based on width & height!
					width = d.dx;
					height = d.dy;
					size = width*height;

					// TODO
					if(size > 400*400)
						className = 'l';
					else if(size > 200*300)
						className = 'm';
					else if(size > 150* 150)
						className = 's';
					else
						className = 'xs';

					if( d.source_el )
						return "<div class='"+className+"'>"+d.source_el.html()+"</div>";

					var day = moment(d.created_at, "YYYY-MM-DD HH:mm");
					return "<div class='"+className+"'><div class='date'>"+day.calendar()+"</div><img onclick='loadNew("+d.id+")' class='item_image' src='"+d.img+"'><h4>"+d.title+"</h4><div class='summary'>"+d.summary+"</div></div>";
				}

	            // node.style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	            node
	        	// .style("background", function(d) { return d.color ? d.color : "#ffffff"; })
	        	.html(set_content)
	        	// .append("test").html("<i>Tests</i>");

	        	node
	        		// .transition().duration(500)
	        		.call(position)
	        	// node.call(position)
	        	;

	        	node.html(set_content);


	        	/*links = [];
	        	console.log(data_items['children'][0]);
	        	for (var i = data_items['children'].length - 1; i >= 0; i--) {
	        		if(i == 0)
	        			continue;

	        		if(data_items['children'][i]['id'] != 1)
	        		{
	        			// links.push({source: data_items['children'][0], target: data_items['children'][i]});// for bundle layout: https://github.com/mbostock/d3/wiki/Bundle-Layout
	        			links.push([data_items['children'][0], data_items['children'][i]]);
	        		}
	        	};


	        	if(links.length)
	        	{


		        	var line = d3.svg.line()
					    .interpolate("bundle")
					    .tension(.85)
					    .x(function(d) { return d.x + d.dx / 2; })
					    .y(function(d) { return d.y + d.dy / 2; });

					var stroke = d3.scale.linear()
						    .domain([0, 1e4])
						    .range(["brown", "steelblue"]);

				    svg.selectAll(".link")
				      // .data(d3.layout.bundle(links)) // requires a parent: as in https://github.com/mbostock/d3/wiki/Bundle-Layout
				      .data(links)
				    .enter().append("path")
				      .attr("class", "link")
				      .attr("d", line)
				      .style("stroke", function(d) { return stroke(d[0].value); })
				      ;
	        	}*/
			      

	        }
	        updateItems();

	        nr = 0;
	        d3.selectAll("#add").on("click", function() {
	        	nr++;
	        	appendMatch({
	        		"match_item_id": nr,
	        		"matches": $("#value").val(),
	        		"summary": "meer",
	        		"title": "a",
	        		"img": "test.jpg"
	        	});
	        });

	    };

	    loadNew = function(item_id)
	    {
	    	$.ajax({
				// dataType: "json",
				method: 'get',
				url: 'http://localhost:8888/api',
				data: {'item_id': item_id},
				success: function(r){
					console.log(r);
				}
			});
	
	    }

	    appendMatch = function(r)
	    {

	    	if(typeof data_items['children'] == 'undefined')
	    	{	
	    		console.log('wait for it...');
	    		return false;
	    	}

	    	data_items['children'].push({
        		"id": r['match_item_id'],
        		"weight": r['matches'],
        		"summary": r['summary'],
        		"title": r['title'],
        		"img": r['image_url'],
        		"created_at": r['created_at']
        	});
        	// console.log(data_items['children']);

        	max_items = 40;
        	if(data_items['children'].length > max_items)
        	{
        		sorter = function(a,b)
				{
					return a['weight'] - b['weight'];
				}

        		console.log('pre', data_items['children']);
				data_items['children'].sort(sorter);
        		toremove = data_items['children'].length - max_items;

        		console.log('post', data_items['children']);

        		for (var i = toremove - 1; i >= 0; i--) {
        			console.log('removed',data_items['children'].shift());
        		};
        	}
        	updateItems();
	    }

	    var start_connection = function()
	    {
	    	console.log('Try connection');
	    	conn = new WebSocket('ws://localhost:8888/ws');
	    	conn.onopen = function(e) {
	    		console.log("Connection established!");
	    	};

	    	$main_item = $('#main_item');

	    var $item;
		// {"id":"9783","version_id":"22651","title":"Captain America, Star-Lord meet 'true superheroes'","media_url":"http:\/\/i2.cdn.turner.com\/cnn\/dam\/assets\/141023151646-captain-america-top-tease.jpg"}
		conn.onmessage = function(msg) {
				// play_sound();
				
				r = JSON.parse(msg.data);

				if(typeof r['action'] == 'undefined')
				{
					console.error('No Action defined');	
					return false;
				}
				if(r['action'] == 'new_item')
				{
					$item = $("<div class='item' id='item-"+r['feed_item_id']+"'>");
					$img = $("<img class='source_image'>");
					$item.append($img);

					
					$img.attr('src', r['media_url']);
					$img.attr('title', r['title']);
					$img.attr('id', 'image-' + r['feed_item_id']);

					$title = $("<h4>").text(r['title']);
					$item.append($title);

					// $main_item.html($item);


					$main_item.empty();
					$wrapper = $("<div class='sift-matches' id='sift-matches-"+r['feed_item_id']+"'>");
					$main_item.append($wrapper);
					createNewTreemap($item, $wrapper);
				}
				else if(r['action'] == 'sift1')
				{
					
					$item.children('img').remove();
					$img = $('<img id="sift-'+r['feed_item_id']+'">').attr('src',r['image_url']);
					$item.prepend($img).addClass('found_features');
				} 
				else if(r['action'] == 'sift_match')
				{
					// div = $("div").html(r['summary']).remove('img');
					// r['summary'] = div.html();
					appendMatch(r);
					// $div = $('<div class="sift-match">');
					// $img = $('<img class="sift-match-img" title='+r['matches']+'>').attr('src',r['image_url']);
					// console.log($img);
					// $('#item-'+r['feed_item_id']).append($img)

					// $title = $("<h4>").text(r['title']);

					// $div.append($img).append($title);
					// $('#sift-matches-'+r['feed_item_id']).append($div);
				} else {
					console.error('unknonw message', r)
				}

				

				// TODO: request matches and get them one by one (as the computer is doing them)
				
				//start parsing
				// parse_get_sift(feed_item);
			};

			// conn.onerror = function(e){
			// 	setTimeout(start_connection, 3000);
			// }
			
			conn.onclose = function(e){
				console.log('Connection closed, retry in 3,5s');
				setTimeout(start_connection, 3500);
			}
		}

		var parse_load_relation = function(feed_item)
		{
			$.ajax({
				dataType: "json",
				url: 'http://localhost:9090/related',
				data: {'item_id': feed_item['id']},
				success: function(r){
					$img = $("<img>");
					
					$('#item-'+r['feed_item_id']).append($img);
					setTimeout(function(){parse_load_relation(this)}.bind(feed_item), 100);
				}
			});

		}
		start_connection();
	});