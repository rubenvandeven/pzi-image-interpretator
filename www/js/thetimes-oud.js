$(function(){
	var elements = []
	var max_weight = 0;
	var options = {
		gridSizeX: 4,
		gridSizeY: 4,
		// maxSizeX: 2,
		// maxSizeY: 2,
		sizes: [
			{className: 'times-item-l', minWeight: 25, maxOcc: 1, size: 4},
			{className: 'times-item-m', minWeight: 10, maxOcc: 1, size: 3},
			{className: 'times-item-s', minWeight: 5, maxOcc: 2, size: 2},
			{className: 'times-item-xs', minWeight: 1, size: 1}
		]
	}

	// string of classnames
	var classNames = '';
	for (var i = options['sizes'].length - 1; i >= 0; i--) {
		classNames += ' ' + options['sizes'][i]['className'];
	};

	var $wrapper = $('#wrapper');

	var addElement = function($element)
	{
		elements.push($element);
		$elements = $(elements);

		// max_weight deprecated? or do class determination based on this (with a logarithmic scale or something)		
		// if($element.data('weight') > max_weight) max_weight = $element.data('weight');

		sorter = function(a,b)
		{
			a = $(a).data('weight');
			b = $(b).data('weight');
			return a < b ? 1 : -1;
		}
		$elements = $elements.sort(sorter);

		assignedClasses = [];
		maxPlaces = options['gridSizeX'] * options['gridSizeY'];
		usePlaces = 0;
		// create array in pythony way
		for (var i = options['sizes'].length - 1; i >= 0; i--) { assignedClasses[i] = 0; }

		doAdd = true;
		// determine sizes
		$elements.each(function(index, el){
			$el = $(el);

			weight = $el.data('weight');

			isUsed = false;
			
			$.each(options['sizes'],function(i, size){
				// console.log(el, size);
				if(weight >= size['minWeight'])
				{
					// skip if there are too many of this size:
					if(typeof size['maxOcc'] != 'undefined' && assignedClasses[i] >= size['maxOcc'])
					{	
						return true; // continue
					}

					// avoid too big items to be added! Add it as a smaller one
					if(usePlaces + size['size'] > maxPlaces)
						return true; // continue

					// the current size is being used!
					usePlaces += size['size'];
					console.log('places',usePlaces);
					isUsed = true;
					assignedClasses[i] += 1;

					if(!$el.hasClass(size['className']))
					{
						console.log('removeclass', el, size['className']);
						$el.removeClass(classNames).addClass(size['className']);
						// make packery attempt to keep the item in place
						// fit_x = undefined;
						// fit_y=undefined;
						// if(size['className'] == 'times-item-l')
						// 	fit_x=fit_y=0;

						// $wrapper.packery( 'fit', el , fit_x, fit_y);
						console.log('FOUND!',el, size['className']);
					}
					return false; // break
				}
			});

			if(!isUsed)
			{
				console.log('remove', index, el);
				$el.find('p').html('removed!');
				// remove elements if needed
				$wrapper.packery( 'remove', el )
				$el.remove();
				elements.splice(index, 1);

				if(true)
				{
					if($element.is($el))
						doAdd = false;
				}
			}
		});

		if(doAdd)
		{
			$wrapper.packery()
					.append( $element )
					.packery('appended', $element)
					.packery('layout');
		}
		else
		{
			console.log('not added',$element);
		}

	}

	var initiate = function()
	{
		$wrapper.packery({
			containerStyle: null,
		  "itemSelector": '.times-item',
		  "columnWidth": ".grid-sizer",
		  // "gutter": ".gutter-sizer",
		  gutter: 0
		});
	}

	initiate();

	nr = 0;
	$('#add').on('click',function(){
		nr++;
		weight = $("#value").val();
		console.log('Add element', weight);
		$element = $('<div class="times-item" data-id="'+nr+'" data-weight="'+weight+'">').data('weight', weight);
		$element.html("<h2>Dit is een test</h2><p>lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, lorem bla bla, .</p>");
		addElement($element);
	});
});