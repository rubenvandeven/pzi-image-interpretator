$(function(){
		var conn = null;

		var $main_item = $('#main_item');

		var images = {};

		var $matches;
		var matches_shuffle;


		var start_connection = function()
		{
			console.log('Try connection');
			conn = new WebSocket('ws://localhost:8888/ws');
			conn.onopen = function(e) {
				console.log("Connection established!");
			};

			// {"id":"9783","version_id":"22651","title":"Captain America, Star-Lord meet 'true superheroes'","media_url":"http:\/\/i2.cdn.turner.com\/cnn\/dam\/assets\/141023151646-captain-america-top-tease.jpg"}
			conn.onmessage = function(msg) {
				// play_sound();
				
				r = JSON.parse(msg.data);

				if(typeof r['action'] == 'undefined')
				{
					console.error('No Action defined');	
					return false;
				}



				if(r['action'] == 'new_item')
				{
					$item = $("<div class='item' id='item-"+r['feed_item_id']+"'>");
					$img = $("<img>");
					$item.append($img);

					
					$img.attr('src', r['media_url']);
					$img.attr('title', r['title']);
					$img.attr('id', 'image-' + r['feed_item_id']);

					$title = $("<h2>").text(r['title']);
					$item.append($title);

					// cleanup and addnew html
					if(matches_shuffle)
						matches_shuffle.destroy();
					if($matches)
					{
						$matches.remove();
						$matches = undefined;
					}
					$main_item.html($item);	

					$matches = $("<div class='sift-matches' id='sift-matches-"+r['feed_item_id']+"'>");
					$main_item.append($matches);

					$matches.shuffle({
						itemSelector: '.sift-match',
						speed: 250,
						easing: 'ease',
						columnWidth: function( containerWidth ) {
							return 200;
							// .box's have a width of 18%
							return 0.18 * containerWidth;
						},
						gutterWidth: function( containerWidth ) {
							return 10;
							// .box's have a margin-left of 2.5%
							return 0.025 * containerWidth;
						}
					});
					matches_shuffle = $matches.data('shuffle');

				}
				else if(r['action'] == 'sift1')
				{
					$img = $('<img id="sift-'+r['feed_item_id']+'">').attr('src',r['image_url']);
					console.log($img);
					$('#item-'+r['feed_item_id']).append($img)
				} 
				else if(r['action'] == 'sift_match')
				{
					$div = $('<div class="sift-match">');
					$div.data('matches', r['matches']);
					$img = $('<img class="sift-match-img" title='+r['matches']+'>').attr('src',r['image_url']);
					console.log($img);
					$('#item-'+r['feed_item_id']).append($img)

					$title = $("<h4>").text('('+r['matches']+') '+r['title']);

					$div.append($img).append($title);
					// $matches = $('#sift-matches-'+r['feed_item_id']);
					$matches.append($div); // append element
					matches_shuffle.appended($div); // notify shuffle
					//reorder:
					reorder_matches();
				} else {
					console.error('unknonw message', r)
				}

				

				// TODO: request matches and get them one by one (as the computer is doing them)
				
				//start parsing
				// parse_get_sift(feed_item);
			};

			// conn.onerror = function(e){
			// 	setTimeout(start_connection, 3000);
			// }
			
			conn.onclose = function(e){
				console.log('Connection closed, retry in 3,5s');
				setTimeout(start_connection, 3500);
			}
		}

		var reorder_matches = function()
		{
			var classes = ['match1', 'match2', 'match3', 'match4','match5','match6','matchN'];
			var classNames = classes.join(' ');
			var matches = $matches.find('div.sift-match');
			sorter = function(a,b)
			{
				a = $(a).data('matches');
				b = $(b).data('matches');
				return a > b ? 1 : -1;
			}
			matches = matches.sort(sorter);
			matches.each(function(i, el){
				a = i+1;
				if(a>6) a = 'N';
				$(el).removeClass(classNames).addClass('match'+a);
			});

			options = {
				by: function($el) {
					return parseInt($el.data('matches'), 10);
				}
			}
			$matches.shuffle('sort', options);

		}

		var parse_load_relation = function(feed_item)
		{
			$.ajax({
				dataType: "json",
				url: 'http://localhost:9090/related',
				data: {'item_id': feed_item['id']},
				success: function(r){
					$img = $("<img>");
					
					$('#item-'+r['feed_item_id']).append($img);
					setTimeout(function(){parse_load_relation(this)}.bind(feed_item), 100);
				}
			});

		}

		start_connection();
	});