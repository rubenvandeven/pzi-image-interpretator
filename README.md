# On digital interpretation of (news) images

Experiment with the presentation of digital interpretation of news images.

What does the world look like, when you only make relations using news images?

## Summary

This project is a research into another way of relating to the world/news. What does it look like?

When we see a photograph (or any other 'text'), we shape our understanding through the relations we construct in our mind.

These associations also contribute to the eliciting of emotions. Some also relate the occurring of emotions with images to ie. automatic mimicry (part of the Autonomic Nervous System - ANS) but the relations between these effects is disputed.

This project tries to present the digital/computer/algorithmic view on interpretation.

## Running

Two parts need to be running simultaneously.

First start the crawler as this creates the newsitems.db file. This process parsess news feeds, pushes items in the database and notifies the server so it run the SIFT algorithm on the image to find its features:
```
python deluge_crawler.py
```

Then start the web and socket server. Which also does/spawns the matching process of the images.
```
python deluge_server.py
```

Then open the browser: http://localhost:8888

And wait for the magic to happen. This might take a while as you will have to wait for a new news item to come in.

Because the crawler tries to notify the server of each new item, it is recommeded to start the processes the other way around after the first run (so first deluge_server then deluge_crawler).