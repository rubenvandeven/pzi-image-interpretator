#!/bin/python
import os
from deluge_server import get_db, prepare_item_images
from shutil import copyfile
from PCV.localdescriptors import sift
'''
Update the images to contain all existing news items
'''

if __name__ == '__main__':
	cur = get_db().cursor()
	cur.execute("SELECT fi.id, fiv.id as feed_item_version_id, fiv.media_url, fiv.title FROM feed_item_versions fiv inner join feed_items fi on fi.id = fiv.feed_item_id WHERE media_url != '' ORDER BY created_at ASC")
	while True:
		item = cur.fetchone()
		if item == None:
			break

		print 'Version:', item['id'], item['feed_item_version_id'], item['title']

		prepare_item_images(item)

	print 'Done'